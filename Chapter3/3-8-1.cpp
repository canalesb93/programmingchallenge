/*
0S,GOMRYPFSU/
*/
#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>
#include <stdlib.h>

using namespace std;

string l="`1234567890-=QWERTYUIOP[]\\ASDFGHJKL;'ZXCVBNM,./";
char ch;

int main ()
{
  int loc, len = l.size();
  string s, output;
  while(getline(cin, s)){
    for(int i = 0; i < s.size(); i++){
      ch = s[i];
      loc = l.find(ch,0);
      if(loc != len){
        s[i]=l[loc-1];
      }
    }
    cout << s << endl;
  }

  return 0;
}
