/*
4 4 3 1 4
5 1 4 2 -1 6
*/
#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>
#include <stdlib.h>

using namespace std;


int main ()
{
  int n, Left, Right;
  while(cin >> n){
    int Numbers[3000] = { 0 };
    cin >> Right;

    for (int i=0; i<n-1; i++)
    {
      Left = Right;
      cin >> Right;
      int diff = abs(Right-Left);
      if (diff >= 1 && diff <= n-1) Numbers[diff] = 1;
    }

    bool Jolly = true;

    for (int i=1; i<n; i++) if (!Numbers[i]) Jolly = false;

    if (Jolly) cout << "Jolly";
    else cout << "Not jolly";

    cout << endl;

  }
  return 0;
}
