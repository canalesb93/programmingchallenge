/*
*/
//QUEUE, INIT, ENQUEUE, DEQUEUE
#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>
#include <stdlib.h>
#define QUEUESIZE 10

using namespace std;

typedef struct {
  int q[QUEUESIZE+1]; //Body
  int first;          //Front
  int last;           //Back
  int count;          //Size
} queue;

void init_queue(queue *q){
  q->first = 0;
  q->last = QUEUESIZE - 1;
  q->count = 0;
}

void enqueue(queue *q, int x){
  if(q->count >= QUEUESIZE)
    cout << endl << "OVERFLOW" << endl;
  else{
    q->last = (q->last+1)%QUEUESIZE;
    q->q[q->last] = x;
    q->count = q->count + 1;
  }
}

int dequeue(queue *q){
  int x;
  if(q->count <= 0) cout << endl << "EMPTY QUEUE" << endl;
  else{
    x= q->q[q->first];
    q->first = (q->first+1) % QUEUESIZE;
    q->count = q->count - 1;
  }
  return x;
}

bool empty(queue *q){
  if(q->count <= 0) return true;
  else return false;
}

void clear_queue( queue *q )
{  
  while(!empty(q)){
    dequeue(q);
  }
}


int main ()
{
  queue q;
  init_queue(&q);
  enqueue(&q, 5);
  enqueue(&q, 4);
  enqueue(&q, 3);
  enqueue(&q, 2);
  enqueue(&q, 1);
  enqueue(&q, 0);
  cout << dequeue(&q) << " ";
  cout << dequeue(&q) << " ";
  cout << dequeue(&q) << " ";
  cout << dequeue(&q) << " ";
  clear_queue(&q);

  cout << dequeue(&q) << " ";
  enqueue(&q, 5);
  cout << dequeue(&q) << " ";
  cout << endl;
  return 0;
}
