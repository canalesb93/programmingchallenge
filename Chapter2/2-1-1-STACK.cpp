/*
*/
//STACK, INIT, PUSH, POP
#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>
#include <stdlib.h>
#define STACKSIZE 10

using namespace std;

typedef struct {
  int s[STACKSIZE+1]; //Body
  int last;           //Back
  int count;          //Size
} stack;

void init_stack(stack *s){
  s->last = 0;
  s->count = 0;
}

void push(stack *s, int x){
  if(s->count >= STACKSIZE)
    cout << "OVERFLOW" << endl;
  else{
    s->last = (s->last+1);
    s->s[s->last] = x;
    s->count = s->count + 1;
  }
}

int pop(stack *s){
  int x;
  if(s->count <= 0) cout << "EMPTY STACK" << endl;
  else{
    x= s->s[s->last];
    s->last = (s->last-1);
    s->count = s->count - 1;
  }
  return x;
}



int main ()
{
  stack s;
  init_stack(&s);
  push(&s, 5);
  push(&s, 4);
  push(&s, 3);
  push(&s, 2);
  push(&s, 1);
  push(&s, 0);
  cout << pop(&s) << " ";
  cout << pop(&s) << " ";
  cout << pop(&s) << " ";
  cout << pop(&s) << " ";
  cout << pop(&s) << " ";
  cout << pop(&s) << " ";
  cout << endl;
  return 0;
}
