/*
Hello World!
*/
#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>
#include <stdlib.h>

using namespace std;

int main ()
{
  cout << "Hello World!" << endl;
  return 0;
}
