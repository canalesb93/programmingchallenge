/*
4 4
*...
....
.*..
....
3 5
**...
.....
.*...
0 0
*/
// MINESWEEPER
#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>
#include <vector>


using namespace std;

int main ()
{
  int n, m, counter = 1;

  // Arrays to easily iterate around value in matrix
  int dx[] = { 1, 1, 1, 0, 0, -1, -1, -1 };
  int dy[] = { 1, -1, 0, 1, -1, 1, 0, -1 };
  char input;
  while(cin >> n >> m){
    if(n == 0 && m == 0){
      return 0;
    } else if(counter > 1){
      // UGLY No newline on last input fix
      cout << endl;
    }
    cout << "Field #" << counter++ << ":" << endl;
    int arr[102][102] = { 0 };
    // Matrix starts at [1][1] to be able to add on negatives
    for(int i = 1; i < n+1; i++){
      for(int j = 1; j < m+1; j++){
        cin >> input;
        if(input == '*'){
          arr[i][j] = -1;
          //Iterates around value
          for(int x = 0; x < 8;x++){
            // If value around bomb is not a bomb add 1
            if(arr[i+dx[x]][j+dy[x]] >= 0){
              arr[i+dx[x]][j+dy[x]] += 1;
            }
          }
        }
      }
    }

    // Output
    for(int i = 1; i < n+1; i++){
      for(int j = 1; j < m+1; j++){
        if(arr[i][j] < 0){
          cout << "*";
        } else {
          cout << arr[i][j];
        }
      }
      cout << endl;
    }
  }

  return 0;
}
