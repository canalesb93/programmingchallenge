/*
10 1
*/
// The 3n+1 problem
#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>
#include <stdlib.h>

using namespace std;

int main ()
{
  unsigned long i, j;
  int maximum, counter;
  while(cin >> i >> j){

    // C style swapping
    bool swapped = false;
    if (i > j) {
      i = i ^ j;
      j = i ^ j;
      i = i ^ j;
      swapped = true;
    }

    maximum = 0;
    for(int current = i; current<=j; current++){
      counter = 1;
      int n = current;
      while(n != 1){
        if(n%2 == 0){
          n/=2;
        } else {
          n*=3;
          n++;
        }
        // cout << n << " ";
        counter++;
      }
      if(counter>maximum)
        maximum = counter;
    }

    if(swapped)
      cout << j << " " << i << " " << maximum << endl;
    else
      cout << i << " " << j << " " << maximum << endl;
  }
    
  return 0;
}
